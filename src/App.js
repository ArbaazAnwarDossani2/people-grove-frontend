import React, { Component } from 'react';
import { Provider } from 'react-redux'
import { createStore, compose, applyMiddleware } from 'redux';
import { autoRehydrate, persistStore } from 'redux-persist';
import createSagaMiddleware from 'redux-saga'
import immutableTransform from 'redux-persist-transform-immutable';
import enUS from 'antd/lib/locale-provider/en_US';
import LocaleProvider from 'antd/lib/locale-provider';
import './App.css';
import Root from './containers/Root';
import peopleGrove from './reducers';
import rootSaga from './saga';
const sagaMiddleware = createSagaMiddleware();

// Creating the Redux Store as peopleGrove.
// Enabling REDUX DEV TOOLS.
// Applying Saga MiddleWare and
// Using Redux-Persist
let store = createStore(
  peopleGrove,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  compose(
    applyMiddleware(sagaMiddleware),
    autoRehydrate()
  )
)
sagaMiddleware.run(rootSaga);

class App extends Component {
  constructor(){
    super();
    this.state={
      initialized: false
    }
  }

  componentWillMount() {
    // Persisting the Immutable Objects in Redux
    persistStore(store, {transforms: [immutableTransform ({
        blacklist: ['filteredCountry']
      })
    ]}, () => {
      this.setState({
        initialized: true,
      })
    });
  }

  render() {
    return (
      <Provider store={store}>
        <div>
          {
            this.state.initialized &&
            <div className="App">
              <LocaleProvider locale={enUS}>
                <Root />
              </LocaleProvider>
            </div>
          }
        </div>
      </Provider>
    );
  }
}

export default App;
