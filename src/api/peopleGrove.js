import request from '../utils/request';
import { BaseUrl } from '../utils/constants';

export const setLogin = data => request('post', `${BaseUrl}/user/login`, data)
export const getTasks = (userId) => request('get', `${BaseUrl}/tasks/getUserTasks/${userId}`)
