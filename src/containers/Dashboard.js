import React from 'react';
import {
  Redirect
} from 'react-router-dom';
import { connect } from 'react-redux';
import { actions as peopleGrove } from '../reducers/peopleGrove';
import Button from 'antd/lib/button';
import Modal from 'antd/lib/modal';
import Input from 'antd/lib/input';
import DatePicker from 'antd/lib/date-picker';
import moment from 'moment';

import TaskList from './components/Tasklist';

class Dashboard extends React.Component {

  _addTask(){
    this.props.setModalVisible(true);
  }

  _handleOk(){
    this.props.setModalVisible(false);
  }

  _handleCancel(){
    this.props.setModalVisible(false);
  }

  _handleChange(e){
    console.log(e.target.value);
  }

  _onOk(e){
    console.log(e);
  }

  _logout(){
    this.props.setUserDetails(null);
    this.props.setLoginStatus(false);
  }

  _onStartDateChange(e, time){
    console.log(e, time);
  }

  _onEndDateChange(e, time){
    console.log(e, time);
  }

  render() {
    if(!this.props.isUserLoggedIn){
      return (
        <Redirect to={"/login"}/>
      )
    }

    // console.log(this.props.userDetail);

    return (
      <div style={{width:'100%'}}>
        {
          this.props.userDetail !== null ?
          <div>
            <p> Hi, {this.props.userDetail.name}</p>
            <Button
              onClick={() => this._addTask()}
              > Add Task </Button>

              <Modal
                title="Add Task"
                visible={this.props.modalVisible}
                onOk={() => this._handleOk()}
                onCancel={() => this._handleCancel()}
                closable={true}
              >
                {
                  this.props.isEdit ?
                  <div>
                    Edit
                  </div>
                  :
                  <div>
                    <Input
                      placeholder="Add Task"
                      onChange={(content) => this._handleChange(content)}
                      />
                      <DatePicker
                        showTime
                        format="YYYY-MM-DD HH:mm:ss"
                        placeholder="Start Time"
                        onChange={this._onStartDateChange}
                        onOk={this._onOk}
                      />
                      <DatePicker
                        showTime
                        format="YYYY-MM-DD HH:mm:ss"
                        placeholder="End Time"
                        onChange={this._onEndDateChange}
                        onOk={this._onOk}
                      />
                  </div>
                }
              </Modal>
              <TaskList />
              <Button
                onClick={() => this._logout()}
                > Logout </Button>

          </div>
          :
          <p>Error</p>
        }
      </div>
    )
  }
}
const mapStateToProps = (state) => ({
  isUserLoggedIn: state.peopleGrove.get('isUserLoggedIn'),
  userDetail: state.peopleGrove.get('userDetail'),
  modalVisible: state.peopleGrove.get('modalVisible'),
  taskContent: state.peopleGrove.get('taskContent'),
})

const mapDispatchToProps = (dispatch) => ({
  setModalVisible : (status) => dispatch(peopleGrove.setModalVisible(status)),
  setUserDetails : (data) => dispatch(peopleGrove.setUserDetails(data)),
  setLoginStatus : (status) => dispatch(peopleGrove.setLoginStatus(status)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
