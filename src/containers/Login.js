import React from 'react';
import {
  Redirect
} from 'react-router-dom';
import { connect } from 'react-redux';
import { actions as peopleGrove } from '../reducers/peopleGrove';

import FacebookLogin from 'react-facebook-login';

class Dashboard extends React.Component {

  _responseFacebook = (response) => {
    let data = {
      user_id : response.userID,
      name : response.name,
      email : response.email,
      gender : response.gender
    }
    this.props.setUserLogin(data);

  }

  render() {
    if(this.props.isUserLoggedIn){
      return (
        <Redirect to={"/"}/>
      )
    }

    return (
      <div style={{display: 'flex', width:'100%', marginTop:40, alignItems:'center', justifyContent:'center', flexDirection:'column'}}>
        <p>Login Into Our System</p>
        <br/>
        <FacebookLogin
          appId="433501623702650"
          autoLoad={true}
          fields="name,email,gender"
          callback={this._responseFacebook} />
      </div>
    )
  }
}
const mapStateToProps = (state) => ({
  isUserLoggedIn: state.peopleGrove.get('isUserLoggedIn'),
})
const mapDispatchToProps = (dispatch) => ({
  setUserLogin : (data) => dispatch(peopleGrove.setUserLogin(data)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
