import React from 'react';
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom';
import Dashboard from './Dashboard';
import Login from './Login';
import history from '../utils/history';

const App = () => (
  <Router history={history}>
    <div style={{width:'100%'}}>
      <Route exact path="/" component={Dashboard} />
      <Route exact path="/login" component={Login} />
    </div>
  </Router>
)

export default App;
