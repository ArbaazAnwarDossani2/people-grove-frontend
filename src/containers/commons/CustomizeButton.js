import React from 'react';
import Button from 'antd/lib/button'

const CustomizeButton = (props) => (
  <Button
    style={Object.assign(props.style || {}, { height: 35, borderRadius: 30, marginBottom: 20, backgroundColor: '#e74c3c', color: '#fff', fontSize: '1rem' })}
    onClick={props.onClick}
  >
    {props.text}
  </Button>
)

export default CustomizeButton;
