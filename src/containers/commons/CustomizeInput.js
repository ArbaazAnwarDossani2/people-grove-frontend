import React from 'react';
import Input from 'antd/lib/input';
import Icon from 'antd/lib/icon';

const CustomizeInput = (props) => (
  <Input
    {...props}
    id={props.id}
    type={props.type ? props.type : 'text'}
    prefix={<Icon type={props.prefix} />}
    style={{ height: 35, marginBottom: 15, width:250}}
    placeholder={props.placeholder}
    disabled={props.disabled ? true : false}
    onChange={props.onChange ? e => props.onChange(e.target.value) : null}
    maxLength={props.maxLength}
  />
)

export default CustomizeInput;
