import React from 'react';
import { connect } from 'react-redux';
import { actions as peopleGrove } from '../../reducers/peopleGrove';
import Table from 'antd/lib/table';
import moment from 'moment';

class Selection extends React.Component {

  componentDidMount(){
    const { getTaskList } = this.props;
    getTaskList();
  }

  render() {
    console.log(this.props.taskList);
    const column = [{
      title: 'Content',
      dataIndex: 'content',
      key: 'content',
    }, {
      title: 'Start Time',
      dataIndex: 'start',
      key: 'start',
    }, {
      title: 'End Time',
      dataIndex: 'end',
      key: 'end',
    }];
    let dataSource=[];
    if(this.props.taskList.length !== 0){
      this.props.taskList.forEach((item,i) => {
        dataSource.push({
          key: i,
          content: item.content,
          start: moment(item.start_date).format("DD/MM/YYYY HH:mm:ss"),
          end: moment(item.end_date).format("DD/MM/YYYY HH:mm:ss"),
        })
      })
    }
    console.log(dataSource);
    return (
      <div className="container" style={{paddingTop: 20}}>
        {
          this.props.taskList.length !== 0 ?
          <div>
            <Table dataSource={dataSource} columns={column} />
          </div>
          :
          <div>
            Loading
          </div>
        }
      </div>
    )
  }
}
const mapStateToProps = (state) => ({
  taskList: state.peopleGrove.get('taskList'),
})
const mapDispatchToProps = (dispatch) => ({
  getTaskList : () => dispatch(peopleGrove.getTaskList()),
})

export default connect(mapStateToProps, mapDispatchToProps)(Selection);
