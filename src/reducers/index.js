import { combineReducers } from 'redux';

import peopleGrove from './peopleGrove';

export default combineReducers({
  peopleGrove
});
