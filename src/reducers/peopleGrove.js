import { fromJS } from 'immutable';

// Actions
export const types = {
  INITIAL: 'app/peopleGrove/INITIAL',
  SET_USER_LOGIN: 'app/peopleGrove/SET_USER_LOGIN',
  SET_LOGIN_STATUS: 'app/peopleGrove/SET_LOGIN_STATUS',
  SET_USER_DETAIL: 'app/peopleGrove/SET_USER_DETAIL',
  SET_MODAL_VISIBLE: 'app/peopleGrove/SET_MODAL_VISIBLE',
  GET_TASK_LIST: 'app/peopleGrove/GET_TASK_LIST',
  SET_TASK_LIST: 'app/peopleGrove/SET_TASK_LIST',
}

// Reducer
export const initialState = fromJS({
  isUserLoggedIn: false,
  userDetail: null,
  modalVisible : false,
  taskList: []
})

export default (state = initialState, action) => {
  switch (action.type) {
    case types.INITIAL:
      return initialState;
    case types.SET_LOGIN_STATUS:
      return state
        .set('isUserLoggedIn', action.status)
    case types.SET_USER_DETAIL:
      return state
        .set('userDetail', action.data)
    case types.SET_MODAL_VISIBLE:
      return state
        .set('modalVisible', action.status)
    case types.SET_TASK_LIST:
      return state
        .set('taskList', action.taskList)
    default:
      return state;
  }
};

// Action Creators
export const actions = {
  setLoginStatus: (status) => ({ type: types.SET_LOGIN_STATUS, status }),
  setUserLogin: (data) => ({ type: types.SET_USER_LOGIN, data }),
  setUserDetails: (data) => ({ type: types.SET_USER_DETAIL, data }),
  setModalVisible: (status) => ({ type: types.SET_MODAL_VISIBLE, status }),
  getTaskList: () => ({ type: types.GET_TASK_LIST}),
  setTaskList: (taskList) => ({ type: types.SET_TASK_LIST, taskList }),
};
