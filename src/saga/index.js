import { takeEvery } from 'redux-saga/effects';

import { types as peopleGroveTypes } from '../reducers/peopleGrove';
import { sagas as peopleGroveSaga } from './peopleGrove';

export default function* rootSaga() {
  yield [
    takeEvery(peopleGroveTypes.SET_USER_LOGIN, peopleGroveSaga.setUserLogin),
    takeEvery(peopleGroveTypes.GET_TASK_LIST, peopleGroveSaga.getTaskList),
  ];
}
