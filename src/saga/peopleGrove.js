import { select, put, call } from 'redux-saga/effects';
import _ from 'lodash';
import { setLogin, getTasks } from '../api/peopleGrove';
import { actions as peopleGroveActions } from '../reducers/peopleGrove';
import { showSuccess, showError } from './../utils/Toast';

function* setUserLogin(action) {
  const res = yield call(setLogin, action.data);
  if(res.status === 200){
    yield put(peopleGroveActions.setUserDetails(res.meta[0]));
    yield put(peopleGroveActions.setLoginStatus(true));
    showSuccess("User Details Saved");
  }
}

function* getTaskList(action) {
  const state = yield select();
  let userDetail = state.peopleGrove.get('userDetail');
  const res = yield call(getTasks, userDetail.user_id);
  // console.log(res);
  if(res.status === 200){
    yield put(peopleGroveActions.setTaskList(res.meta));
  }
  else{
    showError("Unable to Fetch Tasks");
  }
}

export const sagas = {
  setUserLogin,
  getTaskList
};
