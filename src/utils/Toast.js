import Notification from 'antd/lib/notification';

// Ant Design Notification Module for Custom Reuse
export const showError = (title, message) => {
  Notification.error({
    message: title || 'Error',
    description: message,
  });
};

export const showSuccess = (message) => {
  Notification.success({
    message: 'Success',
    description: message,
  });
};
