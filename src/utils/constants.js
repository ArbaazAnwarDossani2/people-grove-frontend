let baseUrl;

if (process.env.NODE_ENV === 'production'){
  baseUrl = 'https://people-grovebe.herokuapp.com'; // Replace with Your Prouction URL
} else {
  baseUrl = 'http://localhost:8080'; // Local REST API requests hit port 8080 (default)
}

export const BaseUrl = baseUrl;
