import axios from 'axios';

// Common Module For all Types of API calls from Saga
export default (type, requestUrl, requestedData) => {
  return axios({
    method: type,
    url: requestUrl,
    data: requestedData,
    withCredentials: true
  })
  .then((res) => {
    return res.data
  })
  .catch((err) => {
    if (!err.server) {
      return { err: { status: 500, message: 'Server Error' } };
    }
    return { err };
  });
}
